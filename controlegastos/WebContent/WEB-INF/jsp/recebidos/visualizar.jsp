<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Controle Gastos</title>
</head>
<body>

<jsp:include page="../menu.jsp"/>

<h1> Controle Recebidos - Visualizar </h1>

<table border="0"> 
       <thead>
              <tr>        
                  <th> Ação </th>
                  <th> Dt. Inicio </th>
                  <th> Dt. Fim </th>
                  <th> Valor </th>
              </tr>
       </thead>
       <tbody> 
              <c:forEach items="${recebidos}" var="recebido"> 
                 <tr>
                     <td> 
                         <a href=" <c:url value="/recebido/${recebido.codigo}/editarTela" />">
                            editar 
                         </a>                         
                     </td>
                     <td> <fmt:formatDate value="${recebido.dataInicio}" pattern="dd/MM/yyyy"/> </td>
                     <td> <fmt:formatDate value="${recebido.dataFim}" pattern="dd/MM/yyyy"/> </td>
                     <td> <fmt:formatNumber currencySymbol="R$" currencyCode="R$" minFractionDigits="2" value="${recebido.valor}"/> </td>
                 </tr>
              </c:forEach>
       </tbody>
</table>

</body>
</html>