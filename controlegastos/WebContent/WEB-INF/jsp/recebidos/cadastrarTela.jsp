<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Controle Gastos</title>
</head>
<body>

<jsp:include page="../menu.jsp"/>

<h1> Controle Gastos - Cadastrar Recebidos </h1>

<form action="<c:url value="/recebido/cadastrarRecebido"/>" method="post">

     <input type="hidden" name="recebido.codigo" value="${recebido.codigo}" />
   
     <p>
         Valor: <input type="text" value="${recebido.valor}" name="recebido.valor">
     </p>
     <p>
        Data Inicio: <input type="text" value="<fmt:formatDate value="${recebido.dataInicio}" pattern="dd/MM/yyyy"/>" name="recebido.dataInicio">
     </p>
     
     <button type="submit"> Inserir </button>

</form>

</body>
</html>