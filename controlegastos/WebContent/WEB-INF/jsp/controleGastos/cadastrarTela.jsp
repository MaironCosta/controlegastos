<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Controle Gastos</title>
</head>
<body>

<jsp:include page="../menu.jsp"/>

<h1> Controle Gastos - Cadastrar Gasto </h1>

<form action="<c:url value="/gastos/cadastrarGasto"/>" method="post">
     <input type="hidden" name="gasto.codigo" value="${gasto.codigo}" />
     <p> Tipo Pagamento:
        <select name="codigoTipoPagamento" >
                <option value="0"> Selecione </option>
                <c:forEach items="${tipoPagamentos}" var="tp">
                   <c:choose>
                      <c:when test="${tp.codigo eq gasto.tipoPagamento.codigo}"> 
                           <option value="${tp.descricao}" selected="selected"> ${tp.descricao} </option>
                      </c:when> 
                      <c:otherwise> 
                          <option value="${tp.descricao}"> ${tp.descricao} </option>                      
                      </c:otherwise>
                   </c:choose>
                </c:forEach>
        </select>
     </p>
     <p>
         Descrição Gasto: <input type="text" value="${gasto.descricao}" name="gasto.descricao">
     </p>
     <p>
        Valor Parcela: <input type="text" value="${gasto.valor}" name="gasto.valor">
     </p>
     <p>
        Quantidade Parcelas: <input type="text" value="${gasto.quantidadeParcelas}" name="gasto.quantidadeParcelas">
     </p>
     <p>
        Data Inicio Pagamento: <input type="text" value="<fmt:formatDate value="${gasto.dataInicioPagamento.time}" pattern="dd/MM/yyyy"/>" name="gasto.dataInicioPagamento">
     </p>
     
     <button type="submit"> Inserir </button>

</form>

</body>
</html>