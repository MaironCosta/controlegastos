<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Controle Gastos</title>
</head>
<body>

<jsp:include page="../menu.jsp"/>
<jsp:useBean id="agora" class="java.util.Date"/>

<h1> Controle Gastos - Visualizar </h1>

<table border="0"> 
       <thead>
              <tr>        
                  <th> TIPO PAGAMENTO </th>
                  <th> DESCRICAO/MES </th>
                  <c:forEach items="${calendars}" var="calendars"> 
                        <th> <fmt:formatDate value="${calendars.time}" pattern="MMM/yyyy"  /> </th>
                  </c:forEach>       
              </tr>
       </thead>
       <tbody>        
              <tr>        
                   <c:forEach items="${gastos}" var="gasto"> 
                      <tr>
                          <td>${gasto.tipoPagamento.descricao}  </td>
                          <td> 
                               <a href="<c:url value="/gastos/${gasto.codigo}/editarTela" />">
                                  ${gasto.descricao}
                               </a>
                          </td>                
                          <c:forEach items="${calendars}" var="calendars"> 
                             <td>
                                 <center>
                                         <c:choose>
                                            <c:when test="${gasto.dataInicioPagamento.time > calendars.time or gasto.dataFimPagamento.time < calendars.time}"> 
                                               -
                                            </c:when > 
                                            <c:otherwise> 
                                               <fmt:formatNumber currencySymbol="R$" minFractionDigits="2" value="${gasto.valor}" />
                                            </c:otherwise>
                                         </c:choose>
                                 </center>
                             </td>
                         </c:forEach>
                      </tr>
                     
                  </c:forEach>       
              </tr>
       </tbody>
       <tfoot>
              
              <tr>
                  <th>  </th>
                  <th> TOTAL </th>    
                     <c:forEach items="${calendars}" var="calendars"> <!-- iterando a caluna jan, fev, ... nov, dez -->
                         <c:set var="total" value="0" />                       
                         <td>
                             <c:forEach items="${gastos}" var="gasto">                                   
                                <c:if test="${gasto.dataInicioPagamento.time < calendars.time or gasto.dataFimPagamento.time > calendars.time}"> 
                                      <c:set var="total" value="${total + gasto.valor}" />
                                </c:if>                                   
                                <c:if test="${gasto.dataInicioPagamento.time > calendars.time or gasto.dataFimPagamento.time < calendars.time}"> 
                                      <c:set var="total" value="${total - gasto.valor}" />
                                </c:if>                                                         
                             </c:forEach>                             
                             -----------------
                             <center>
                                    <fmt:formatNumber currencySymbol="R$" minFractionDigits="2" value="${total}" /> 
                             </center>  
                         </td>
                  </c:forEach>    
              </tr>
              <tr>
                  <th>  </th> 
                  <th> SALDO </th>   
                     <c:forEach items="${calendars}" var="calendars"> <!-- iterando a caluna jan, fev, ... nov, dez -->
                         <c:set var="total" value="0" />                       
                         <td>
                             <c:forEach items="${gastos}" var="gasto">                                   
                                <c:if test="${gasto.dataInicioPagamento.time < calendars.time or gasto.dataFimPagamento.time > calendars.time}"> 
                                      <c:set var="total" value="${total + gasto.valor}" />
                                </c:if>                                   
                                <c:if test="${gasto.dataInicioPagamento.time > calendars.time or gasto.dataFimPagamento.time < calendars.time}"> 
                                      <c:set var="total" value="${total - gasto.valor}" />
                                </c:if>                                                         
                             </c:forEach>                             
                             -----------------
                             <center>
                                    <fmt:formatNumber currencySymbol="R$" minFractionDigits="2" value="${recebido.valor - total}" /> 
                             </center>  
                         </td>
                  </c:forEach> 
              </tr>
       </tfoot>
</table>

</body>
</html>