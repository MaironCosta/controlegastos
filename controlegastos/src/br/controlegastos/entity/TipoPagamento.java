package br.controlegastos.entity;

public enum TipoPagamento {

	BOLETO (1, "BOLETO"),
	CARTAO_FNAC (2, "CARTAO FNAC"),
	CARTAO_CITI_BANK (3, "CARTAO CITI BANK"),
	CARTAO_NUBANK (5, "CARTAO NUBANK"),
	DEBITO (4, "DEBITO");
	
	private int codigo;
	private String descricao;
	
	private TipoPagamento(int codigo, String descricao) {
		// TODO Auto-generated constructor stub
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
