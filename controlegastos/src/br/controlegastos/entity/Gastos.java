package br.controlegastos.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.controlegastos.utils.ILocalidade;
import br.controlegastos.utils.UtilsDate;

@Entity
@Table(name = "GASTOS")
@SequenceGenerator(name="GASTOS_SEQUENCE",sequenceName="GASTOS_SEQ")
public class Gastos implements IEntity, Serializable, Comparable<Gastos>,
		Comparator<Gastos>, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="CODIGO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GASTOS_SEQUENCE")
	private Long codigo;
	
	@Column(name = "DESCRICAO")
	private String descricao;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "DATA_INICIO_PAGAMENTO")
	private Calendar dataInicioPagamento;
	
	@Temporal(value = TemporalType.DATE)
	@Column(name = "DATA_FIM_PAGAMENTO")
	private Calendar dataFimPagamento;
	
	@Column(name = "QUANTIDADE_PARCELAS")
	private int quantidadeParcelas;
	
	@Enumerated(value = EnumType.STRING)
	@Column(name = "TIPO_PAGAMENTO")
	private TipoPagamento tipoPagamento;
	
	@Column(name = "VALOR")
	private BigDecimal valor;
	
//	@Column(name = "PARCELA")
//	private int parcela;

	public Gastos() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Calendar getDataInicioPagamento() {
		return dataInicioPagamento;
	}

	public void setDataInicioPagamento(Calendar dataInicioPagamento) {
		this.dataInicioPagamento = dataInicioPagamento;
	}

	public void setDataFimPagamento(Calendar dataFimPagamento) {
		this.dataFimPagamento = dataFimPagamento;
	}

	public Calendar getDataFimPagamento() {
		return dataFimPagamento;
	}

	public int getQuantidadeParcelas() {
		
		if (quantidadeParcelas == 0) {
			quantidadeParcelas = 1;
		}
		
		return quantidadeParcelas;
	}

	public void setQuantidadeParcelas(int quantidadeParcelas) {
		this.quantidadeParcelas = quantidadeParcelas;
	}

	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public int getDifMeses() {
		
		int difMes = 0;
		
		if (this.getDataFimPagamento() != null) {

			difMes = UtilsDate.diferencaMes(Calendar.getInstance(ILocalidade.DEFAULT_LOCALE), 
					                        (Calendar)this.getDataFimPagamento().clone());
			
		}		
		
		return difMes;
	}

	@Override
	public int compare(Gastos o1, Gastos o2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int compareTo(Gastos o) {
		// TODO Auto-generated method stub
		return 0;
	}

//	public int getParcela() {
//		return parcela;
//	}
//
//	public void setParcela(int parcela) {
//		this.parcela = parcela;
//	}
	
	private String getDataInicioPagamentoFormatada() {
		return new SimpleDateFormat("dd/MM/yyyy").format(dataInicioPagamento.getTime());
	}

	@Override
	public String toString() {
		return "Gastos [codigo=" + codigo + ", descricao=" + descricao
				+ ", data=" + this.getDataInicioPagamentoFormatada() + ", quantidadeParcelas="
				+ quantidadeParcelas + ", tipoPagamento=" + tipoPagamento
				+ ", valor=" + valor + "]";
	}


}
