package br.controlegastos.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RECEBIDO")
@SequenceGenerator(name="RECEBIDO_SEQUENCE",sequenceName="RECEBIDO_SEQ")
public class Recebido implements IEntity, Cloneable, Comparable<Recebido>,
		Comparator<Recebido>, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODIGO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RECEBIDO_SEQUENCE")
	private Long codigo;
	
	@Column(name = "VALOR")
	private BigDecimal valor;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "DATA_INICIO")
	private Date dataInicio;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "DATA_FIM")
	private Date dataFim;

	public Recebido() {
		// TODO Auto-generated constructor stub
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	@Override
	public int compare(Recebido o1, Recebido o2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int compareTo(Recebido o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toString() {
		return "Recebido [codigo=" + codigo + ", valor=" + valor
				+ ", dataInicio=" + dataInicio + ", dataFim=" + dataFim + "]";
	}

}
