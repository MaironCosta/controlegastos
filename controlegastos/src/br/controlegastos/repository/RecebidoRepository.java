package br.controlegastos.repository;

import java.util.List;

import br.controlegastos.entity.Recebido;

public interface RecebidoRepository  extends IRepositoryGeneric<Recebido>{

	public Recebido getUltimo(boolean isAtivo);
	
	public List<Recebido> getUltimos(int maxResult, Boolean isAtivo);

}
