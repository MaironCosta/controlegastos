package br.controlegastos.repository;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import br.controlegastos.entity.Recebido;

@SuppressWarnings({"unchecked"})
public class RecebidoRepositoryImpl extends RepositoryGeneric<Recebido> implements RecebidoRepository {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final Session session;

	public RecebidoRepositoryImpl(Session session) {
		super(session);
		// TODO Auto-generated constructor stub
		
		this.session = session;
	}

	@Override
	public Recebido getUltimo(boolean isAtivo) {
		// TODO Auto-generated method stub
		
		StringBuilder completeQuery = new StringBuilder();
		completeQuery.append("FROM Recebido r WHERE r.dataFim ");
		
		completeQuery.append((isAtivo)?" is null ":" is not null");
		
		Query query = session.createQuery(completeQuery.toString());
		query.setMaxResults(1);
		
		return (Recebido) query.uniqueResult();
	}
	
	@Override
	public List<Recebido> getUltimos(int maxResult, Boolean isAtivo) {
		// TODO Auto-generated method stub
		
		StringBuilder completeQuery = new StringBuilder();
		completeQuery.append("FROM Recebido r ");
		
		if (isAtivo != null) {
			
			completeQuery.append((isAtivo)?" WHERE r.dataFim is null ":" WHERE r.dataFim is not null");
			
		}
		
		completeQuery.append(" ORDER BY r.dataFim DESC ");
		
		Query query = session.createQuery(completeQuery.toString());
		query.setMaxResults(maxResult);
		
		return query.list();
	}

}
