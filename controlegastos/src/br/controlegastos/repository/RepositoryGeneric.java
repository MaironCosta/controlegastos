package br.controlegastos.repository;

import java.io.Serializable;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import br.controlegastos.entity.IEntity;

@SuppressWarnings({"unchecked"})
public class RepositoryGeneric<T> implements IRepositoryGeneric<T>, Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7086859252950189103L;
	
	Logger log = Logger.getLogger(RepositoryGeneric.class);	
	
	private final Session session;	
		
	public RepositoryGeneric(Session session) {
		// TODO Auto-generated constructor stub
		
		super();
		this.session = session;
				
	}

	@Override
	public T persistence(T t) {
		// TODO Auto-generated method stub
				
		try {
						
			session.persist(t);
			session.flush();
			session.refresh(t);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return t;
	}

	@Override
	public T merge(T t) {
		// TODO Auto-generated method stub
		
		try {
			
			session.merge(t);
			session.flush();
			session.refresh(t);
			
		} catch (Exception e) {
			// TODO: handle exception
		
			e.printStackTrace();
		
		}
		
		return t;
	}

	@Override
	public void remove(T t) {
		// TODO Auto-generated method stub

		try {
			
		//	entityManager.evict(t);
			Query query = session.createQuery("DELETE FROM " + t.getClass().getSimpleName().toUpperCase() + " WHERE codigo = " + ((IEntity)t).getCodigo());
			query.executeUpdate();
		//	session.flush();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public Collection<T> findAll(Class<?> classe) {
		// TODO Auto-generated method stub

		Collection<T> result = null;
		
		try {
			
			Query query = session.createQuery("FROM " + classe.getName());			
			result = query.list();
			
		} catch (Exception e) {
			// TODO: handle exception
			// // log.info(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public Collection<T> findAll(Class<?> classe, boolean isAtivo) {
		
		Collection<T> result = null;
		
		try {
			
			Query query = session.createQuery("FROM " + classe.getName() + " WHERE isAtivo is " + isAtivo);			
			result = query.list();
			
		} catch (Exception e) {
			// TODO: handle exception
			// log.info(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Override
	public T findByField(String fieldName, Class<?> classe, Object value){
		
		T result = null;
		
		try {
			
			Query query = session.createQuery("from " + classe.getName() + " x WHERE x." + fieldName + "= :value");
			query.setParameter("value", value);
			query.setMaxResults(1);
			
			result = (T) query.uniqueResult();
			
		} catch (Exception e) {
			// TODO: handle exception
			// log.info(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public Collection<T> findByField(Class<?> classe, Object value, String fieldName){
		
		Collection<T> result = null;
		
		try {
			Query query = session.createQuery("from " + classe.getName() + " x WHERE x." + fieldName + " = :value");
			query.setParameter("value", value);
			
			result = query.list();
			
		} catch (Exception e) {
			// TODO: handle exception
			// log.info(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public Collection<T> findByLikeField(Class<?> classe, String fieldName, String value){
		
		Collection<T> result = null;
		
		try {
			
			Query query = session.createQuery("FROM " + classe.getName() + " x WHERE x." + fieldName + " like :value");
			query.setParameter("value", "%" + value + "%");
			
			result = query.list();
			
		} catch (Exception e) {
			// TODO: handle exception
			// log.info(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}
	
}
