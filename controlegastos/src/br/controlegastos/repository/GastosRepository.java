package br.controlegastos.repository;

import java.util.Calendar;
import java.util.List;

import br.controlegastos.entity.Gastos;

public interface GastosRepository extends IRepositoryGeneric<Gastos> {

	public List<Gastos> getByData (Calendar dataInicial, Calendar dataFinal);
	
	public List<Gastos> getByData();
	
}
