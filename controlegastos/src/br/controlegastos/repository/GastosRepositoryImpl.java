package br.controlegastos.repository;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import br.controlegastos.entity.Gastos;
import br.controlegastos.utils.ILocalidade;
import br.controlegastos.utils.UtilsDate;

@SuppressWarnings("unchecked")
public class GastosRepositoryImpl extends RepositoryGeneric<Gastos> implements GastosRepository {

	private Logger log = Logger.getLogger(GastosRepositoryImpl.class);	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Session session;

	public GastosRepositoryImpl(Session session) {
		super(session);

		this.session = session;
		
	}

	@Override
	public List<Gastos> getByData(Calendar dataInicial, Calendar dataFinal) {
		
		log.info("getByData: " + UtilsDate.calendarToString(dataInicial) + " e " + UtilsDate.calendarToString(dataFinal));
		
		Query query = session.createQuery("FROM Gastos g WHERE g.dataFimPagamento BETWEEN :dataInicial AND :dataFinal ORDER BY g.tipoPagamento.descricao, g.descricao");
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);

		List<Gastos> gastos = query.list();
		
		return gastos;
		
	}	
	
	@Override
	public List<Gastos> getByData() {		
		
		Query query = session.createQuery("FROM Gastos g WHERE g.dataFimPagamento >= :hoje ORDER BY g.tipoPagamento.descricao, g.descricao");
		query.setParameter("hoje", Calendar.getInstance(ILocalidade.DEFAULT_LOCALE));

		List<Gastos> gastos = query.list();
		
		return gastos;
		
	}

}
