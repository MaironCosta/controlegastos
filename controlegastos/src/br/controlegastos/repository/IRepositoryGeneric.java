package br.controlegastos.repository;

import java.util.Collection;


public interface IRepositoryGeneric<T> {

	T persistence (T t);
	T merge (T t);
	void remove (T t);
	Collection<T> findAll (Class<?> classe);
	public Collection<T> findAll(Class<?> classe, boolean isAtivo);
	public T findByField(String fieldName, Class<?> classe, Object value);
	public Collection<T> findByField(Class<?> classe, Object value, String fieldName);
	public Collection<T> findByLikeField(Class<?> classe, String fieldName, String value);	

}
