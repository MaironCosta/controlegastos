package br.controlegastos.exception;

import java.util.ArrayList;
import java.util.List;

public class ValidacaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<String> mensagem = new ArrayList<String>();

	public ValidacaoException() {
		// TODO Auto-generated constructor stub
	}

	public ValidacaoException(String message) {
		super(message);
		this.mensagem.add(message);
	}

	public ValidacaoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ValidacaoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ValidacaoException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ValidacaoException(List<String> mensagem) {
		super();
		this.mensagem = mensagem;
	}

}
