package br.controlegastos.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.controlegastos.config.HibernateUtil;
import br.controlegastos.entity.IEntity;
import br.controlegastos.exception.ValidacaoException;
import br.controlegastos.repository.IRepositoryGeneric;
import br.controlegastos.repository.RepositoryGeneric;


@SuppressWarnings({"unchecked"})
public class SuperBusiness {

	private Logger log = Logger.getLogger(SuperBusiness.class);	
	
	private List<String> msg = new ArrayList<String>();	
						
	public SuperBusiness() {
		// TODO Auto-generated constructor stub
	}
	
	public List<String> getMsg() {
		return msg;
	}

	public void setMsg(List<String> msg) {
		this.msg = msg;
	}
	
	public Session getConection(Session session){
		
		log.info("Abrindo Conexao");
		
		if (session == null || (session != null && !session.isOpen())) {
			
			log.info("CONEXAO FECHADA. ABRINDO CONEXAO");
			session = HibernateUtil.getSessionFactory().openSession();

			session.setCacheMode(CacheMode.IGNORE);
			
		}
		
		return session;
	}
	
	public IEntity createUpdate(IEntity obj, final Session session) throws ValidacaoException {
		// TODO Auto-generated method stub
				
		IRepositoryGeneric<Object> repositoryGeneric = new RepositoryGeneric(session);
		Transaction transaction = session.beginTransaction();
		
		try {
			
			if(((obj.getCodigo() == null)?repositoryGeneric.persistence(obj):repositoryGeneric.merge(obj)) == null){
				
				transaction.rollback();
				
			} else {
				
				transaction.commit();
				
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			transaction.rollback();
		} finally {
			
		//	this.fecharConexoes(session);
			
		}
		
		return (IEntity) obj;
	}
	
	public void fecharConexoes(Session session) {

		log.info("FECHANDO CONEXAO");
		
		if (session != null && session.isOpen()) {
			
			session.clear();
			session.close();
			
		}
		
	}
	
	public void closeConection() {

		if (!HibernateUtil.getSessionFactory().isClosed()) 
			HibernateUtil.getSessionFactory().close();
	}
		
}
