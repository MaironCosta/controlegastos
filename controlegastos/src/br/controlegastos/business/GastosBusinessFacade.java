package br.controlegastos.business;

import java.util.Calendar;
import java.util.List;

import br.controlegastos.entity.Gastos;

public interface GastosBusinessFacade extends IBusinessFacade<Gastos> {
	
	public List<Gastos> getByData (Calendar dataInicial, Calendar dataFinal);
	
	public List<Gastos> getByData();

}
