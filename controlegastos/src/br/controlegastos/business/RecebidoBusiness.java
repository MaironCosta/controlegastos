package br.controlegastos.business;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;

import br.controlegastos.entity.Recebido;
import br.controlegastos.exception.ValidacaoException;
import br.controlegastos.repository.RecebidoRepository;
import br.controlegastos.repository.RecebidoRepositoryImpl;
import br.controlegastos.utils.ILocalidade;

public class RecebidoBusiness extends SuperBusiness implements RecebidoBusinessFacade {

	private Session session;
	
	private RecebidoRepository recebidoRepository;
	
	public RecebidoBusiness() {
		// TODO Auto-generated constructor stub
		
		session = super.getConection(session);
		
	}

	@Override
	public void iniciar() {
		// TODO Auto-generated method stub

	}

	@Override
	public Recebido persist(Recebido obj) throws ValidacaoException, IllegalArgumentException {
		// TODO Auto-generated method stub
		
		recebidoRepository = new RecebidoRepositoryImpl(session);
		
		Recebido recebidoAntigo = recebidoRepository.getUltimo(true);
		if (recebidoAntigo != null) {
			
			recebidoAntigo.setDataFim(Calendar.getInstance(ILocalidade.DEFAULT_LOCALE).getTime());
			super.createUpdate(recebidoAntigo, session);
			
		}
		
		obj.setDataFim(null);
		super.createUpdate(obj, session);

		super.fecharConexoes(session);
		
		return obj;
	}

	@Override
	public void delete(Recebido obj) throws ValidacaoException,
			IllegalArgumentException {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<Recebido> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Recebido findByCod(long codigo) throws ValidacaoException, IllegalArgumentException {
		// TODO Auto-generated method stub
		
		recebidoRepository = new RecebidoRepositoryImpl(session);
		
		Recebido recebido = (Recebido) recebidoRepository.findByField("codigo", Recebido.class, codigo);
		
		this.fecharConexoes(session);
		
		return recebido;
	}

	public Recebido getUltimo(boolean isAtivo) throws ValidacaoException, IllegalArgumentException {
		// TODO Auto-generated method stub

		recebidoRepository = new RecebidoRepositoryImpl(session);

		Recebido recebido = recebidoRepository.getUltimo(isAtivo);
		
		this.fecharConexoes(session);
		
		return recebido;
	}
	
	@Override
	public List<Recebido> getUltimos(int maxResult, Boolean isAtivo) {
		// TODO Auto-generated method stub
		
		recebidoRepository = new RecebidoRepositoryImpl(session);
		
		List<Recebido> recebidos = recebidoRepository.getUltimos(maxResult, isAtivo);
		
		this.fecharConexoes(session);
		
		return recebidos;
	}

}
