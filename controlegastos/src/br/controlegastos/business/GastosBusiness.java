package br.controlegastos.business;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import br.controlegastos.entity.Gastos;
import br.controlegastos.exception.ValidacaoException;
import br.controlegastos.repository.GastosRepository;
import br.controlegastos.repository.GastosRepositoryImpl;

public class GastosBusiness extends SuperBusiness implements GastosBusinessFacade {
	
	private Logger log = Logger.getLogger(GastosBusiness.class);	
	
	private Session session;
	
	private GastosRepository gastosRepository;

	public GastosBusiness() {
		// TODO Auto-generated constructor stub
		
		session = super.getConection(session);
		
	}

	@Override
	public void iniciar() {
		// TODO Auto-generated method stub

	}

	public Gastos persist_BACKUP_02_11_2014(Gastos obj) throws ValidacaoException, IllegalArgumentException {
		// TODO Auto-generated method stub
		
		if (obj == null) {
			
			throw new IllegalArgumentException("Informe o gasto");
			
		}
		
		Gastos gastos = null;
		for (int i = 0; i < obj.getQuantidadeParcelas(); i++) {
			
			gastos = new Gastos();
			gastos.setDataInicioPagamento((Calendar)obj.getDataInicioPagamento().clone());
			gastos.getDataInicioPagamento().add(Calendar.MONTH, i);
			
			log.info("Data: " + new SimpleDateFormat("dd/MM/yyyy").format(gastos.getDataInicioPagamento().getTime()));
			
			gastos.setDescricao(obj.getDescricao());
			gastos.setQuantidadeParcelas(obj.getQuantidadeParcelas());
		//	gastos.setParcela(i + 1); 
			gastos.setTipoPagamento(obj.getTipoPagamento());
			gastos.setValor(obj.getValor().divide(new BigDecimal(obj.getQuantidadeParcelas()), RoundingMode.HALF_UP));
			
		//	log.info(gastos.toString());
			
			super.createUpdate(gastos, session);
			
			super.fecharConexoes(session);
			
		}
		
		return obj;
	}
	
	@Override
	public Gastos persist(Gastos obj) throws ValidacaoException, IllegalArgumentException {
		// TODO Auto-generated method stub
		
		if (obj == null) {
			
			throw new IllegalArgumentException("Informe o gasto");
			
		}
		
		obj.setDataFimPagamento((Calendar)obj.getDataInicioPagamento().clone());
		obj.getDataFimPagamento().add(Calendar.MONTH, (obj.getQuantidadeParcelas() - 1));
		obj.getDataFimPagamento().set(Calendar.DAY_OF_MONTH, obj.getDataFimPagamento().getActualMaximum(Calendar.DAY_OF_MONTH));
		
		super.createUpdate(obj, session);
		
		this.fecharConexoes(session);
		
		return obj;
	
	}

	@Override
	public void delete(Gastos obj) throws ValidacaoException,
			IllegalArgumentException {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<Gastos> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Gastos findByCod(long codigo) throws ValidacaoException, IllegalArgumentException {
		// TODO Auto-generated method stub
		
		gastosRepository = new GastosRepositoryImpl(session);
		Gastos gasto = (Gastos) gastosRepository.findByField("codigo", Gastos.class, codigo);
		
		this.fecharConexoes(session);
		
		return gasto;
	}

	@Override
	public List<Gastos> getByData(Calendar dataInicial, Calendar dataFinal) {
		// TODO Auto-generated method stub
		
		gastosRepository = new GastosRepositoryImpl(session);
		
		List<Gastos> gastos = gastosRepository.getByData(dataInicial, dataFinal);
		
		this.fecharConexoes(session);
				
		return gastos;
	}

	@Override
	public List<Gastos> getByData() {
		// TODO Auto-generated method stub
		
		gastosRepository = new GastosRepositoryImpl(session);
		
		List<Gastos> gastos = gastosRepository.getByData();
		
		this.fecharConexoes(session);
		
		return gastos;
	}
	
}
