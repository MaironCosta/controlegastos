package br.controlegastos.business;

import java.util.List;

import br.controlegastos.entity.Recebido;
import br.controlegastos.exception.ValidacaoException;

public interface RecebidoBusinessFacade extends IBusinessFacade<Recebido> {

	public Recebido getUltimo(boolean isAtivo) throws ValidacaoException, IllegalArgumentException;
	
	public List<Recebido> getUltimos(int maxResult, Boolean isAtivo);
	
}
