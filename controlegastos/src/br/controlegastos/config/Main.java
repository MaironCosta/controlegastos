package br.controlegastos.config;


import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

@SuppressWarnings({"deprecation"})
public class Main {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		Logger log = Logger.getLogger(Main.class);
		
		final String pathCfg = "br/controlegastos/config/hibernate.cfg.xml";

		try {
			
			new SchemaExport(new AnnotationConfiguration().
					configure(pathCfg)).
					create(true, true);

			log.info("TABELAS GERADAS COM SUCESSO!");
			log.info("LOADING DATABASE STARTED!");
	//		GeradorEntity.gerar();
			log.info("LOADING DATABASE COMPLETE!");

		} catch (HibernateException he) {
			// TODO Auto-generated catch block
			he.printStackTrace();
			log.info(he.getMessage());
		}catch (Exception e) {
			// TODO: handle exception
		   
			e.printStackTrace();
		}	
	}

}
