package br.controlegastos.config;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;


public class HibernateUtil {
	
    private static final SessionFactory sessionFactory;

/*    static {
        try {
            
           sessionFactory = new AnnotationConfiguration().configure("config/hibernate.cfg.xml").buildSessionFactory();
        	
        } catch (Throwable ex) {
            
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }finally{
        	
           if(!getSessionFactory().isClosed()) getSessionFactory().close();
        	
        }
    }*/
    
    static {
    	
        try {
        	
        /*	Configuration configuration = new Configuration();
        	configuration.configure("config/hibernate.cfg.xml");
        	
        	ServiceRegistryBuilder serviceRegistryBuilder = new ServiceRegistryBuilder().
        			applySettings(configuration.getProperties());
            
           sessionFactory = configuration.buildSessionFactory(serviceRegistryBuilder.buildServiceRegistry());*/
        	
        	Configuration configuration = new Configuration();
            configuration.configure("br/controlegastos/config/hibernate.cfg.xml");
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        	
        } catch (Throwable ex) {
            
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }finally{
        	
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
}
