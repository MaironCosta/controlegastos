package br.controlegastos.utils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Locale;

import br.controlegastos.business.RecebidoBusiness;
import br.controlegastos.business.RecebidoBusinessFacade;
import br.controlegastos.entity.Recebido;
import br.controlegastos.exception.ValidacaoException;

public class GeradorEntity {

	public GeradorEntity() {
		// TODO Auto-generated constructor stub
	}
	
	public void gerarRecebido () {
		
		Recebido recebido = new Recebido();
		recebido.setDataInicio(Calendar.getInstance(new Locale("pt", "BR")).getTime());
		recebido.setValor(new BigDecimal(1900));
		
		RecebidoBusinessFacade recebidoBusinessFacade = new RecebidoBusiness();
		try {
			
			recebidoBusinessFacade.persist(recebido);
			
		} catch (IllegalArgumentException | ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		
		GeradorEntity geradorEntity = new GeradorEntity();
		geradorEntity.gerarRecebido();	
		
	}

}
