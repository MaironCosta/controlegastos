package br.controlegastos.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.InputMismatchException;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Valida {

	static TimeZone COUNTRY_TIMEZONE = TimeZone.getTimeZone("America/Sao_Paulo");		
	
	public static boolean isEmpty(Collection<?> list){
		
		if(list == null)
			return true;
		else if(list.isEmpty()) 
			return true;
		else if(list.size() < 1)
			return true;
		
		return false;
	}
	
	public static boolean isEmpty(String string){
		
		if(string == null)
			return true;
		else if ("".equals(string.trim()))
			return true;
		else if (string.length() < 1)
			return true;
		
		return false;
	}
	
	public static boolean isEmpty(Calendar date){
		
		if(date == null)
			return true;

		return false;
	}	
	
	public static boolean validaNome(String name){
		
		if(Valida.isEmpty(name))
			return false;
		if(name.length() < 3)
			return false;
		else if(name.length() > 60)
			return false;
		
		return true;
	}

	/**
	 * Verifica se � Domingo ou Segunda-feira
	 * 
	 * @author Mairon
	 * @since 01/03/2013
	 * 
	 * */
	public static boolean isFinalDeSemana (Calendar data) {

		System.out.println("Dia da Semana: " + data.get(Calendar.DAY_OF_WEEK));
		if (data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
			return true;		
		else if (data.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
			return true;
		
		return false;
	}
	
	public static boolean validarCPF(String cpf){

		if (cpf == null) {
			
			return false;
			
		}
		
		cpf = cpf.replace(".", "").replace("-", "");
		System.out.println("CPF: " + cpf);
		
		for (int i = 0; i <= (cpf.length() - 1); i++) {

			try {
				
				Integer.valueOf(String.valueOf(cpf.charAt(i)));
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return false;
			}
					
		}
		
		if ("11111111111".equals(cpf) || "22222222222".equals(cpf) || "33333333333".equals(cpf) 
			|| "44444444444".equals(cpf) || "55555555555".equals(cpf) || "66666666666".equals(cpf)
            || "77777777777".equals(cpf) || "88888888888".equals(cpf) || "99999999999".equals(cpf) 
            || "00000000000".equals(cpf)) {
			
			return false;
			
		}

		int somaPrimeiroDigito = 0;
		int multiplicador = 10;

		for (int i = 0; i <= (cpf.length() -3); i++) {

			somaPrimeiroDigito += (Integer.valueOf(String.valueOf(cpf.charAt(i))) * multiplicador);
			multiplicador--;			
		}
		
		int primeiroDigito = (somaPrimeiroDigito % 11 == 0 || somaPrimeiroDigito % 11 == 1)?
				(somaPrimeiroDigito % 11): 11 - (somaPrimeiroDigito % 11);

		// CALCULAR O SEGUNDO DIGITO

		int somaSegundoDigito = 0;
		multiplicador = 10;

		for (int i = 1; i <= (cpf.length() -2); i++) {

			somaSegundoDigito += (Integer.valueOf(String.valueOf(cpf.charAt(i))) * multiplicador);
	//		System.out.println("Caracter: " + cpf.charAt(i) + " Multiplicador: " + multiplicador + " somaPrimeiroDigito: " + somaSegundoDigito);
			multiplicador--;					
	//		somaSegundoDigito += (multiplicador == 2)? (primeiroDigito * multiplicador): 0;			
			
		}

		int segundoDigito = (somaSegundoDigito % 11 == 0 || somaSegundoDigito % 11 == 1)?
				(somaSegundoDigito % 11):
					11 -(somaSegundoDigito % 11);

		System.out.println("Primeiro Digito: " + primeiroDigito + " Segundo Digito: " + segundoDigito);
		int cpfVerificador01 = Integer.valueOf(String.valueOf(cpf.subSequence((cpf.length()-2), (cpf.length()-1))));
		int cpfVerificador02 = Integer.valueOf(String.valueOf(cpf.subSequence((cpf.length()-1), cpf.length()))); 

		if ((primeiroDigito == 0 || primeiroDigito == 1 || primeiroDigito == cpfVerificador01) &&
				(segundoDigito == 0 || segundoDigito == 1 || segundoDigito == cpfVerificador02)) {
			
			return true;
			
		}
		
		return false;
	}
	
	public static boolean isEmail(String email) {  
		
	    Pattern pattern = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");   
	    Matcher matcher = pattern.matcher(email);   
	    
	    return matcher.find();   
	} 
	
/*	
 *   javascript
 * 
 * function IsEmail(email){

	    var exclude=/[^@-.w]|^[_@.-]|[._-]{2}|[@.]{2}|(@)[^@]*1/;
	    var check=/@[w-]+./;
	    var checkend=/.[a-zA-Z]{2,3}$/;
	    if(((email.search(exclude) != -1)||(email.search(check)) == -1)||(email.search(checkend) == -1)){return false;}
	    else {return true;}
	}*/

	public static boolean validarCNPJ(String CNPJ) { 
		// considera-se erro CNPJ's formados por uma sequencia de numeros iguais 
		if (CNPJ.equals("00000000000000") || 
				CNPJ.equals("11111111111111") || 
				CNPJ.equals("22222222222222") || 
				CNPJ.equals("33333333333333") || 
				CNPJ.equals("44444444444444") || 
				CNPJ.equals("55555555555555") || 
				CNPJ.equals("66666666666666") || 
				CNPJ.equals("77777777777777") || 
				CNPJ.equals("88888888888888") || 
				CNPJ.equals("99999999999999") || 
				(CNPJ.length() != 14)) {

			return(false); 
		}
		char dig13, dig14; 
		int sm, i, r, num, peso; 
		// "try" - protege o c�digo para eventuais erros de conversao de tipo (int) 
		try { // Calculo do 1o. Digito Verificador 
			sm = 0; 
			peso = 2; 

			for (i=11; i>=0; i--) { 
				// converte o i-�simo caractere do CNPJ em um n�mero: // por exemplo, transforma o caractere '0' no inteiro 0 
				// (48 eh a posi��o de '0' na tabela ASCII) 
				num = (int)(CNPJ.charAt(i) - 48); 
				sm = sm + (num * peso); 
				peso = peso + 1; 
				if (peso == 10) { 

					peso = 2; 

				} 

			} 

			r = sm % 11; 
			if ((r == 0) || (r == 1)){ 

				dig13 = '0'; 

			} else {

				dig13 = (char)((11-r) + 48); 

			}// Calculo do 2o. Digito Verificador 

			sm = 0; 
			peso = 2; 

			for (i=12; i>=0; i--) {

				num = (int)(CNPJ.charAt(i)- 48); 
				sm = sm + (num * peso); 
				peso = peso + 1; 
				if (peso == 10) { 

					peso = 2; 

				} 

			} 

			r = sm % 11; 

			if ((r == 0) || (r == 1)) { 

				dig14 = '0'; 

			} else { 

				dig14 = (char)((11-r) + 48); 

			}// Verifica se os d�gitos calculados conferem com os d�gitos informados. 

			if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13))) { 

				return(true); 

			} else { 

				return(false); 

			} 

		} catch (InputMismatchException erro) { 

			return(false); 

		} 
		
	} 


	//	Leia mais em: Validando o CNPJ em uma Aplica��o Java 
    // http://www.devmedia.com.br/validando-o-cnpj-em-uma-aplicacao-java/22374#ixzz2kq693QNJ
	
	
}