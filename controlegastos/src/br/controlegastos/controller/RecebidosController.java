package br.controlegastos.controller;

import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.controlegastos.business.RecebidoBusiness;
import br.controlegastos.business.RecebidoBusinessFacade;
import br.controlegastos.entity.Recebido;
import br.controlegastos.exception.ValidacaoException;

@Resource
public class RecebidosController implements IController<Recebido> {

	private final Result result;
	private RecebidoBusinessFacade recebidoBusinessFacade;
	
	public RecebidosController(Result result) {
		// TODO Auto-generated constructor stub
		this.result = result;
	}

	@Override
	@Path(value = "/recebido/cadastrarRecebidoTela")
	public void cadastrarTela() {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Path(value = "/recebido/cadastrarRecebido")
	public void cadastrar(Recebido recebido) {
		// TODO Auto-generated method stub
		
		recebidoBusinessFacade = new RecebidoBusiness();
		
		try {
			
			recebidoBusinessFacade.persist(recebido);
			
		} catch (IllegalArgumentException | ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result.redirectTo(this).cadastrarTela();
	}
	
	@Override
	@Path(value = "/recebido/visualizar")
	public void visualizar() {
		
		recebidoBusinessFacade = new RecebidoBusiness();
		
		List<Recebido> recebidos = recebidoBusinessFacade.getUltimos(5, null);
				
		result.include("recebidos", recebidos);
		
	}

	@Override
	@Path(value = "/recebido/{codigo}/editarTela")
	public void editarTela(Long codigo) {
		// TODO Auto-generated method stub
		
		recebidoBusinessFacade = new RecebidoBusiness();
		Recebido recebido = null;
		try {
			
			recebido = recebidoBusinessFacade.findByCod(codigo);
			
		} catch (IllegalArgumentException | ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result.include("recebido", recebido).redirectTo(this).cadastrarTela();
		
	}

}
