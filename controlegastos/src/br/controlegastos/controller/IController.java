package br.controlegastos.controller;

public interface IController<T> {

	public void cadastrarTela ();
	public void cadastrar (T t);
	public void visualizar ();
	public void editarTela (Long codigo);
	
}
