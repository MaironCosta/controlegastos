package br.controlegastos.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.controlegastos.business.GastosBusiness;
import br.controlegastos.business.GastosBusinessFacade;
import br.controlegastos.business.RecebidoBusiness;
import br.controlegastos.business.RecebidoBusinessFacade;
import br.controlegastos.entity.Gastos;
import br.controlegastos.entity.Recebido;
import br.controlegastos.entity.TipoPagamento;
import br.controlegastos.exception.ValidacaoException;
import br.controlegastos.utils.ILocalidade;

@Resource
public class ControleGastosController implements IController<Gastos> {

	private final Result result;
	private final HttpServletRequest request;
	
	private GastosBusinessFacade gastosBusinessFacade;
	private RecebidoBusinessFacade recebidoBusinessFacade;
	
	public ControleGastosController(Result result, HttpServletRequest request) {
		// TODO Auto-generated constructor stub
		this.result = result;
		this.request = request;
	}
	@Path(value = "/home")
	public void home () {
		
	}
	
	@Path(value = "/gastos/cadastrarGastoTela")
	public void cadastrarTela () {
		
		List<TipoPagamento> tipoPagamentos = Arrays.asList(TipoPagamento.values());
		
		result.include("tipoPagamentos", tipoPagamentos);
		
	}
	
	@Path(value = "/gastos/cadastrarGasto")
	public void cadastrar (Gastos gasto) {
		
		String codigoTipoPagamento = request.getParameter("codigoTipoPagamento");
		
		TipoPagamento tipoPagamento = TipoPagamento.valueOf(codigoTipoPagamento);
		
		gasto.setTipoPagamento(tipoPagamento);
		
		gastosBusinessFacade = new GastosBusiness();
		
		try {
			
			gastosBusinessFacade.persist(gasto);
		
		} catch (IllegalArgumentException | ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result.include("mensagem", "Cadastrado com Sucesso!").redirectTo(this).cadastrarTela();
		
	}
	
	public void visualizar_BKP_08_02_2015 () {
		
		gastosBusinessFacade = new GastosBusiness();
		recebidoBusinessFacade = new RecebidoBusiness();
		
		final int difMes = 10;

		Calendar dataInicial = Calendar.getInstance(ILocalidade.DEFAULT_LOCALE);
		dataInicial.set(Calendar.DAY_OF_MONTH, 1);
		
		Calendar dataFinal = Calendar.getInstance(ILocalidade.DEFAULT_LOCALE);
		dataFinal.add(Calendar.MONTH, difMes);
		dataFinal.set(Calendar.DAY_OF_MONTH, dataFinal.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		List<Calendar> calendars = new ArrayList<Calendar>();
				
		for (int i = 0; i < difMes; i++) {
			
			Calendar calendar = Calendar.getInstance(ILocalidade.DEFAULT_LOCALE);
			calendar.add(Calendar.MONTH, i);
			calendars.add(calendar);
			
		}
		
//		List<Gastos> gastos = gastosBusinessFacade.getByData(dataInicial, dataFinal);
		List<Gastos> gastos = gastosBusinessFacade.getByData();
		Recebido recebido = null;
		try {
			
			recebido = recebidoBusinessFacade.getUltimo(true);
			
		} catch (IllegalArgumentException | ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result.include("gastos", gastos)
		      .include("calendars", calendars)
		      .include("recebido", recebido)
		      .include("quantidadeIteracao", difMes - 1);
		
	}
	
	@Path(value = "/gastos/visualizar")
	public void visualizar () {
		
		gastosBusinessFacade = new GastosBusiness();
		recebidoBusinessFacade = new RecebidoBusiness();
		
		final int difMes = 6;

		Calendar dataInicial = Calendar.getInstance(ILocalidade.DEFAULT_LOCALE);
		dataInicial.set(Calendar.DAY_OF_MONTH, 1);
		
		Calendar dataFinal = Calendar.getInstance(ILocalidade.DEFAULT_LOCALE);
		dataFinal.add(Calendar.MONTH, difMes);
		dataFinal.set(Calendar.DAY_OF_MONTH, dataFinal.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		List<Calendar> calendars = new ArrayList<Calendar>();
				
		for (int i = 0; i < difMes; i++) {
			
			Calendar calendar = Calendar.getInstance(ILocalidade.DEFAULT_LOCALE);
			calendar.add(Calendar.MONTH, i);
			calendars.add(calendar);
			
		}
		
//		List<Gastos> gastos = gastosBusinessFacade.getByData(dataInicial, dataFinal);
		List<Gastos> gastos = gastosBusinessFacade.getByData();
		Recebido recebido = null;
		try {
			
			recebido = recebidoBusinessFacade.getUltimo(true);
			
		} catch (IllegalArgumentException | ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result.include("gastos", gastos)
		      .include("calendars", calendars)
		      .include("recebido", recebido)
		      .include("quantidadeIteracao", difMes - 1);
		
	}

	
	@Path(value = "/gastos/{codigo}/editarTela")
	public void editarTela (Long codigo) {

		gastosBusinessFacade = new GastosBusiness();
		Gastos gasto = null;
		try {
			
			gasto = gastosBusinessFacade.findByCod(codigo);
			
		} catch (IllegalArgumentException | ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result.include("gasto", gasto).redirectTo(this).cadastrarTela();
		
	}

}
